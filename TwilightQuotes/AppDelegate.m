//
//  AppDelegate.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewControllers.h"
#import "AuthorsViewControllers.h"
#import "SettingViewControllers.h"
#import "MoreViewControllers.h"
#import "QuotesListViewControllers.h"
#import "FavoritesQuotesListViewControllers.h"
@implementation AppDelegate

@synthesize window = _window;

@synthesize tabbarcontroller=_tabbarcontroller;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    qList=[QuotesLists Instance];
    if (![qList isLoaded]) {
        if (![self checkRechability]) {
            UIAlertView *someError = 
            [[UIAlertView alloc] initWithTitle: @"Warning" 
                                       message: @"Your device is not connected to the internet, please connect to use all features of the application." 
                                      delegate: self 
                             cancelButtonTitle: @"Ok" 
                             otherButtonTitles: nil];
            
            [someError show];
            
        }else{
            [qList xmlParse];
        }
        
        
    }

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    QuotesListViewControllers *homeView=[QuotesListViewControllers quotesListViewControllerWithQuoteList:qList];
    AuthorsViewControllers *authorsView=[[AuthorsViewControllers alloc]initWithNibName:@"AuthorsView" bundle:nil];
    FavoritesQuotesListViewControllers *favoriteView=[[FavoritesQuotesListViewControllers alloc]initWithNibName:@"FavoritesQuotesListView" bundle:nil];
     SettingViewControllers *settingsView=[[SettingViewControllers alloc]initWithNibName:@"SettingsView" bundle:nil];
     MoreViewControllers *moreView=[[MoreViewControllers alloc]initWithNibName:@"MoreView" bundle:nil];
    
    UINavigationController *homeController=[[UINavigationController alloc]initWithRootViewController:homeView];
    
    UINavigationController *authorsControllers=[[UINavigationController alloc]initWithRootViewController:authorsView];
    
    UINavigationController *favoriteController=[[UINavigationController alloc]initWithRootViewController:favoriteView];
    
    UINavigationController *settingsControllers=[[UINavigationController alloc]initWithRootViewController:settingsView];
    
    UINavigationController *moreController=[[UINavigationController alloc]initWithRootViewController:moreView];
    
    
    homeController.navigationBar.barStyle=UIBarStyleBlackOpaque;
    favoriteController.navigationBar.barStyle=UIBarStyleBlackOpaque;
    authorsControllers.navigationBar.barStyle=UIBarStyleBlackOpaque;
    settingsControllers.navigationBar.barStyle=UIBarStyleBlackOpaque;
    moreController.navigationBar.barStyle=UIBarStyleBlackOpaque;
    self.tabbarcontroller=[[UITabBarController alloc]init];
    self.tabbarcontroller.viewControllers = [NSArray arrayWithObjects:homeController,favoriteController,authorsControllers,settingsControllers,moreController, nil];

    self.window.rootViewController = self.tabbarcontroller;
   // UIImage* tabitem = [UIImage imageNamed:@"background1.png"];
   // [self.window setBackgroundColor:[UIColor colorWithPatternImage:tabitem ]];
    [self.window makeKeyAndVisible];
    return YES;
}
-(void)LoadData{
    
}
-(BOOL) checkRechability {    
    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)) {
        return NO;
    }
    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) 
        
    {
        if ([alertView.title isEqualToString:@"Warning"]) 
        {
            exit(1);  
        }
        // [self applicationWillTerminate:nil];  
        NSLog(@"Cancel Click");
    }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
  
}

@end
