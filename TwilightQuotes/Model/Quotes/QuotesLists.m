//
//  QuoteList.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QuotesLists.h"
static QuotesLists *sharedObject = nil;
@implementation QuotesLists
@synthesize quotesList=_quotesList;
@synthesize searchResultQuotesList=_searchResultQuotesList;

+ (id)Instance
{
    if(sharedObject==nil)
    {
        sharedObject = [[super allocWithZone:NULL] init];   
        sharedObject.quotesList=nil;
    }
    return sharedObject;
}

-(BOOL)xmlParse{
    BOOL success;
   
    if(_quotesList==nil||_quotesList.count<=0)
    {
        
        NSURL *url = [[NSURL alloc] initWithString:QUOTE_XML_URL];
        NSXMLParser *XmlParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        
        //Initialize the delegate.
        
        
        //Set delegate
        [XmlParser setDelegate:self];
        
        //Start parsing the XML file.
        success = [XmlParser parse];
        
        if(success)
            NSLog(@"No Errors");
        else
            NSLog(@"Error Error Error!!!");
    }
    
    return success;
}
#pragma mark - XML parser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName
    attributes:(NSDictionary *)attributeDict {
    
    if([elementName isEqualToString:@"Quoteset"]) {
        _quotesList=[[NSMutableArray alloc]init];
    }
    else if([elementName isEqualToString:@"Quote"]) {
        aQuotes = [[Quotes alloc] init];
    }
        
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if(!currentElementValue)
        currentElementValue = [[NSMutableString alloc] initWithString:string];
    else
        [currentElementValue appendString:string];
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if([elementName isEqualToString:@"Quoteset"])
        return;

    
    if([elementName isEqualToString:@"Quote"]) {
        [_quotesList addObject:aQuotes];
        
        
        
    }
    else
    {
        NSString *Value= [NSString stringWithString: currentElementValue];
        
        if ([elementName isEqualToString:@"Id"]) 
        {
            Value=[Value stringByTrimmingCharactersInSet:
                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            aQuotes.quoteId = (int)[[NSString stringWithString:Value] intValue];
        }
        else if([elementName isEqualToString:@"Text"])
        {
            aQuotes.quoteText =Value;
        }
                
        
        
    }
    
    currentElementValue = nil;
}

-(Quotes *)GetQuoteById:(int)QId
{
   
    if(QId>0)	
    {
        for (int i=0; i<self.quotesList.count; i++) {
            Quotes * item=[self.quotesList objectAtIndex :i];
            if(item.quoteId==QId)
            {
                return item;
            }
        }
    }
    return nil;
}

- (int) indexOfQuote:(Quotes*) quote {
   
    int retval = 0;
    for (int i=0; i<self.quotesList.count; i++) {
        Quotes * item=[self.quotesList objectAtIndex :i];
        if(item.quoteId == quote.quoteId){
            retval = i;
            break;
        }
    }
    return  retval;
}

- (Quotes*) quoteAtIndex:(int) index {
    
    Quotes *q = [self.quotesList objectAtIndex:index];
    
    return q;
}

- (int) loadedCount {
    return self.quotesList.count;
}

-(BOOL)isLoaded{
    if (self.quotesList==nil) {
        return NO;
    }else {
        if (self.quotesList.count<=0) {
            return NO;
        }
    }
    return YES;
    
}
- (NSMutableArray*)search:(NSString*)text{
    _searchResultQuotesList = [[NSMutableArray alloc] init];
    if(text.length <= 0){
        _searchResultQuotesList = _quotesList;
        return nil;
    }
    for (Quotes *c in _quotesList) {
        if([c contains:text]){
            [_searchResultQuotesList addObject:c];
        }
    }
    NSLog(@"Search :%i",_searchResultQuotesList.count);
    return _searchResultQuotesList;
   
    
}

@end
