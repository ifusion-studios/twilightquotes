//
//  Quotes.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Quotes.h"

@implementation Quotes
@synthesize quoteId=_quoteId;
@synthesize quoteText=_quoteText;

- (BOOL) contains:(NSString*) searchString{
    if (searchString.length <= 0) {
        return YES;
    }
    BOOL contains = NO;
    NSString *quoteText = [self.quoteText stringByConvertingHTMLToPlainText];
    NSRange range = [quoteText rangeOfString:searchString options:NSCaseInsensitiveSearch range:NSMakeRange(0, searchString.length + 1)];
    
    if(range.location != NSNotFound){
        contains = YES;
    }

        return contains;
}
@end
