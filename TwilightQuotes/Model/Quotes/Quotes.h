//
//  Quotes.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+HTML.h"
@interface Quotes : NSObject{
    NSString *_quoteText;
    int _quoteId;
}
- (BOOL) contains:(NSString*) searchString;
@property(nonatomic,retain,readwrite)NSString *quoteText;
@property(nonatomic,assign,readwrite)int quoteId;

@end
