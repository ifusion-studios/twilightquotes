//
//  QuoteList.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quotes.h"
#import "Global.h"
@interface QuotesLists : NSObject<NSXMLParserDelegate>{
   
    NSMutableString *currentElementValue;
    
    Quotes *aQuotes;
    NSMutableArray *_quotesList;
    NSMutableArray *_searchResultQuotesList;

}
-(BOOL)xmlParse;
-(Quotes *)GetQuoteById:(int)QId;
- (int) indexOfQuote:(Quotes*) quote;
- (Quotes*) quoteAtIndex:(int) index;
- (int) loadedCount;
+(id)Instance;
-(BOOL)isLoaded;
- (NSMutableArray*)search:(NSString*)text;
@property(nonatomic,readwrite,retain)NSMutableArray *quotesList;
@property(nonatomic,readwrite,retain)NSMutableArray *searchResultQuotesList;
@end
