//
//  FavoriteQuoteList.m
//  TwilightQuotes
//
//  Created by Samir Kha on 01/09/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FavoriteQuoteList.h"
static NSString* KEY = @"FavoriteCourse";

@implementation FavoriteQuoteList
@synthesize favoriteQuoteList=_favoriteQuoteList;
+(void)addAndRemoveToFavorites:(Quotes *)quote{
    
    NSObject *obj= [UserDefaults GetValueFromUserDefaults:KEY];
    NSString *val=@"";
    bool flag=NO;
    if([obj isKindOfClass:[NSString class]])
    {
        NSString *str = [NSString stringWithFormat:@"%@",obj];
        
        NSArray *firstSplit = [str componentsSeparatedByString:@","];
        for (int i=0; i<firstSplit.count; i++) {
            NSString * item=[firstSplit objectAtIndex:i];
            if(item.length>0)
            {
                if((int)[item intValue]!=quote.quoteId)
                {
                    val=[NSString stringWithFormat:@"%@,%@",val, item];
                }
                else
                {
                    flag=YES;
                    
                }
            }
        }
    }
    if(!flag)
    {
        val=[NSString stringWithFormat:@"%@,%d",val, quote.quoteId];
    }
    [UserDefaults  WriteInUserDefaults:KEY:val] ;
    //[self GetFavoriteCourse];
}

-(NSMutableArray *)getFavoriteList{
    
    return [self GetFavoriteQuotes];
}

- (Quotes *)quoteAtIndex:(NSUInteger)index{
    return (Quotes*)[[self getFavoriteList] objectAtIndex:index];
}

- (NSUInteger) loadedCount{
    return [_favoriteQuoteList count];
}
-(Quotes *)GetQuoteById:(int)QId{
   // QuoteList *_quotesList=[[QuoteList alloc]init];
    NSMutableArray *quotes = [[QuotesLists Instance]quotesList];
    if(QId>0)
    {
        for (int i=0; i<[quotes count]; i++) {
            Quotes * item=[quotes objectAtIndex:i];
            if(item.quoteId==QId)
            {
                return item;
            }
        }
    }
    return nil;
}

- (BOOL) isFavorite:(Quotes*) quote{
    NSMutableArray *list = [self GetFavoriteQuotes];
    for (Quotes *c in list) {
        if(c.quoteId == quote.quoteId){
            return YES;
        }
    }
    return NO;
}

-(NSMutableArray*)GetFavoriteQuotes
{
    //QuoteList *_quoteList=[[QuoteList alloc]init];
   NSMutableArray *quotes = [[QuotesLists Instance]quotesList];
    if([quotes count]>0)
    {
        _favoriteQuoteList=[[NSMutableArray alloc] init];
        NSObject *obj= [UserDefaults GetValueFromUserDefaults:KEY];
        if([obj isKindOfClass:[NSString class]])
        {
            NSString *str = [NSString stringWithFormat:@"%@",obj];
            
            NSArray *firstSplit = [str componentsSeparatedByString:@","];
            for (int i=0; i<firstSplit.count; i++) {
                NSString * item=[firstSplit objectAtIndex:i];
                if(item.length>0)
                {
                    Quotes *c=[self GetQuoteById:(int)[item intValue]];
                    if(c!=nil)
                    {
                        [_favoriteQuoteList addObject:c];
                    }
                }
            }
        }
    }
    return _favoriteQuoteList;
}


@end
