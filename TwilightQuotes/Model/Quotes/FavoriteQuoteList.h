//
//  FavoriteQuoteList.h
//  TwilightQuotes
//
//  Created by Samir Kha on 01/09/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuotesLists.h"
#import "Quotes.h"
#import "UserDefaults.h"
@interface FavoriteQuoteList : NSObject{
    NSMutableArray *_favoriteQuoteList;
}
@property(nonatomic, readwrite, retain) NSMutableArray *favoriteQuoteList;


+ (void)addAndRemoveToFavorites:(Quotes*)quote;
-(Quotes *)GetQuoteById:(int)QId;

-(NSMutableArray*)GetFavoriteQuotes;
- (Quotes*) quoteAtIndex:(NSUInteger) index;

- (NSUInteger) loadedCount;

- (BOOL) isFavorite:(Quotes*) quote;

@end
