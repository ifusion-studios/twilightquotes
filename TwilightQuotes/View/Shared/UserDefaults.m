//
//  UserDefaults.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserDefaults.h"

@implementation UserDefaults
+(BOOL)WriteInUserDefaults:(NSString *)Key:(NSObject *)Value
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if (prefs) 
    {
        [prefs setObject:Value forKey:Key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return YES;
    }
    return NO;
}

+(NSObject *)GetValueFromUserDefaults:(NSString *)Key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSObject * val = nil;
    if (prefs) 
    {
        val = [prefs objectForKey:Key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (val==nil) {
            return nil;
        }
    }
    
    return val;
}

@end
