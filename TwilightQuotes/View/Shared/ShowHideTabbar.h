//
//  ShowHideTabbar.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowHideTabbar : NSObject
+(void) hideTabBar:(UITabBarController *) tabbarcontroller ;
+(void) showTabBar:(UITabBarController *) tabbarcontroller ;
@end
