//
//  UserDefaults.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaults : NSObject
+(BOOL)WriteInUserDefaults:(NSString *)Key:(NSObject *)Value;
+(NSObject *)GetValueFromUserDefaults:(NSString *)Key;
@end
