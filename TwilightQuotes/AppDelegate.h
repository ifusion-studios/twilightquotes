//
//  AppDelegate.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "QuotesLists.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    QuotesLists *qList;
}

@property (strong, nonatomic) UIWindow *window;

@property(strong,nonatomic)UITabBarController *tabbarcontroller;

- (BOOL) checkRechability;
-(void)LoadData;
@end
