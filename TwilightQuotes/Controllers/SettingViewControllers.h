//
//  SettingViewControllers.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CALayer.h>
#import "ColorPickerViewController.h"
#import "CMFontSelectTableViewController.h"
#import "JPImagePickerController.h"
#import "UserDefaults.h"
@interface SettingViewControllers :UIViewController<ColorPickerViewControllerDelegate, JPImagePickerControllerDelegate,JPImagePickerControllerDataSource,UINavigationControllerDelegate> {
    IBOutlet UISlider *fontSize;
    int imgno;
}
@property(retain,nonatomic)IBOutlet UIButton *selectedImage;
@property(retain,nonatomic)IBOutlet UIButton *selectedFont;
@property (readwrite,nonatomic,retain) IBOutlet UIView *colorQuote;
@property(retain,nonatomic)IBOutlet UIImageView *selectedImg;
@property(retain,nonatomic)NSString *val;


-(IBAction)selectColorofQuotes;
-(IBAction)selectFont;
-(IBAction)SizeChange:(id)sender;
+(SettingViewControllers*)settingViewController;
@end
