//
//  HomeViewControllers.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeViewControllers.h"

@implementation HomeViewControllers
+(HomeViewControllers*)homeViewController
{
    HomeViewControllers *result = [[HomeViewControllers alloc] initWithNibName:@"HomeView" bundle:[NSBundle mainBundle]];
    
    return result;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //self.title=@"Home";
        
        self.title = NSLocalizedString(@"All Quotes", @"All Quotes");
        self.navigationItem.title=@"Home";
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    qList=[QuotesLists Instance];
    if (![qList isLoaded]) {
        if (![self checkRechability]) {
            UIAlertView *someError = 
            [[UIAlertView alloc] initWithTitle: @"Warning" 
                                       message: @"Your device is not connected to the internet, please connect to use all features of the application." 
                                      delegate: self 
                             cancelButtonTitle: @"Ok" 
                             otherButtonTitles: nil];
            
            [someError show];
            
        }else{
            [self performSelectorInBackground:@selector(LoadData) withObject:nil];
            
            
        }

        
    }
   
  //  NSLog(@"%@",qList.quotesList);
    
    
}
-(void)LoadData{
     [qList xmlParse];
}
-(BOOL) checkRechability {    
    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)) {
        return NO;
    }
    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) 
        
    {
        if ([alertView.title isEqualToString:@"Warning"]) 
        {
            exit(1);  
        }
        // [self applicationWillTerminate:nil];  
        NSLog(@"Cancel Click");
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [ShowHideTabbar hideTabBar:self.tabBarController];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(IBAction)Quotes{
    
      [self.navigationController pushViewController:[QuotesListViewControllers quotesListViewControllerWithQuoteList:qList] animated:YES];
     
    
}
-(IBAction)Favorites{
    
    
    [self.navigationController pushViewController:[FavoritesQuotesListViewControllers favoriteQuotesListViewController] animated:YES];
    
}
-(IBAction)Settings{
    
   
    
    [self.navigationController pushViewController:[SettingViewControllers settingViewController] animated:YES];
    
}
-(IBAction)More{
    
    [self.navigationController pushViewController:[MoreViewControllers moreViewController] animated:YES];
    
}
@end
