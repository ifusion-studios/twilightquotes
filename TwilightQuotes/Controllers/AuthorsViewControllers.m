//
//  AuthorsViewControllers.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AuthorsViewControllers.h"

@implementation AuthorsViewControllers
+(AuthorsViewControllers*)authorsViewController
{
    AuthorsViewControllers *result = [[AuthorsViewControllers alloc] initWithNibName:@"AuthorsView" bundle:[NSBundle mainBundle]];
    
    return result;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"Charactors" ;
        self.tabBarItem.image=[UIImage imageNamed:@"charctor.png"];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
   // qList=[[QuotesLists alloc]init];
     
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
//-(void)Navigate{
//    [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
//}

-(IBAction)BellaClick{
    [[QuotesLists Instance]search:@"Bella"];
    
   [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
     
   
}
-(IBAction)EdwardClick{
    [[QuotesLists Instance]search:@"Edward"];
   [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
}
-(IBAction)JacobClick{
    [[QuotesLists Instance]search:@"Jacob"];
   [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
}
-(IBAction)JamesClick{
    [[QuotesLists Instance]search:@"James"];
  [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
}
-(IBAction)CharlieClick{
    [[QuotesLists Instance]search:@"Charlie"];
   [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
}
-(IBAction)MikeClick{
    [[QuotesLists Instance]search:@"Mike"];
    [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
}
-(IBAction)JessicaClick{
    [[QuotesLists Instance]search:@"Jessica"];
    [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
}
-(IBAction)AliceClick{
    [[QuotesLists Instance]search:@"Alice"];
   [self.navigationController pushViewController:[CharactorWiseQuoteListViewControllers quotesListViewControllerWithQuoteList:[[QuotesLists Instance]searchResultQuotesList]] animated:YES];
}
@end
