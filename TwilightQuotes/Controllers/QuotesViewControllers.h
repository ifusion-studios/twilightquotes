//
//  QuotesViewControllers.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "Quotes.h"
#import "UserDefaults.h"
#import "QuotesLists.h"
#import "FavoriteQuoteList.h"
#import "ActivityIndicator.h"
#import "SocialClient.h"
@class FacebookClient ;
@class TwitterClient ;
@interface QuotesViewControllers : UIViewController<SocialClientDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIActionSheetDelegate>{
    Quotes *_quote;
    IBOutlet UITextView *txtQuote;
    QuotesLists *qList;
    int _index;
    
    FacebookClient *facebookClient ;
    TwitterClient *twitterClient ;
}
@property(readwrite, nonatomic,retain) Quotes *quote;
@property(retain,nonatomic)IBOutlet UIImageView *backimage;
@property(assign, nonatomic) int index;
@property(nonatomic, retain, readwrite)IBOutlet UIButton *addFav,*removeFav;
+(QuotesViewControllers*)quotesViewControllerWithQuotes:(Quotes*)quote ;
-(void) BindData;
- (IBAction)handleSwipeLeft:(UISwipeGestureRecognizer *)sender;
- (IBAction)handleSwipeRight:(UISwipeGestureRecognizer *)sender;
-(void)swipe;
-(void) iMessage;
- (void)MailSend;
- (IBAction)Share;
-(IBAction)addFavorites;
-(IBAction)removeFavorites;
@end
