//
//  HomeViewControllers.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuotesListViewControllers.h"
#import "SettingViewControllers.h"
#import "FavoritesQuotesListViewControllers.h"
#import "MoreViewControllers.h"
#import "ShowHideTabbar.h"
#import "QuotesLists.h"
#import "Reachability.h"
#import "FavoriteQuoteList.h"
@interface HomeViewControllers : UIViewController{
    QuotesLists *qList;
    FavoriteQuoteList *favQList;
}
+(HomeViewControllers*)homeViewController;
-(IBAction)Quotes;
-(IBAction)Favorites;
-(IBAction)Settings;
-(IBAction)More;
- (BOOL) checkRechability;
-(void)LoadData;
@end
