//
//  CharactorWiseQuoteListViewControllers.m
//  TwilightQuotes
//
//  Created by Samir Kha on 03/09/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CharactorWiseQuoteListViewControllers.h"


@implementation CharactorWiseQuoteListViewControllers

@synthesize qList;
+(CharactorWiseQuoteListViewControllers*)quotesListViewControllerWithQuoteList:(NSMutableArray*)quoteList
{
    CharactorWiseQuoteListViewControllers *result = [[CharactorWiseQuoteListViewControllers alloc] initWithNibName:@"CharactorWiseQuoteListView" bundle:[NSBundle mainBundle]];
    result.qList=quoteList;
    return result;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"All Quotes", @"All Quotes");
        self.tabBarItem.image=[UIImage imageNamed:@"list.png"];
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"Back"
                                              style:UIBarButtonItemStyleBordered
                                              target:nil
                                              action:nil];
        self.navigationItem.backBarButtonItem = backBarButtonItem;
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
   // qList =[[QuotesLists alloc]init];
   // NSLog(@"Search :%@",qList.searchResultQuotesList);
   // NSLog(@"Search :%i",qList.searchResultQuotesList.count);
//    UIViewController *vc_1;
//    vc_1 = [self.tabBarController.viewControllers objectAtIndex:1];
//    [vc_1 tabBarItem].enabled = NO;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
//    UIViewController *vc_1;
//    vc_1 = [self.tabBarController.viewControllers objectAtIndex:1];
//    [vc_1 tabBarItem].enabled = YES;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [ShowHideTabbar showTabBar:self.tabBarController];
    [super viewWillAppear:animated];
//    UIViewController *vc_1;
//    vc_1 = [self.tabBarController.viewControllers objectAtIndex:1];
//    [vc_1 tabBarItem].enabled = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    UIViewController *vc_1;
//    vc_1 = [self.tabBarController.viewControllers objectAtIndex:1];
//    [vc_1 tabBarItem].enabled = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.

    return [qList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    QuotesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"QuotesCell" owner:self options:nil];
        
        for (id currentObject in topLevelObjects){
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell =  (QuotesCell *) currentObject;
                break;
            }
        }
        
        UIFont *f;
        NSString *fontSize=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedFontSize"];
        if(fontSize!=nil && fontSize.length>0)
        {
            f=[UIFont fontWithName:@"geeza pro" size:(float)[fontSize floatValue]];
            
        }
        else
        {
            f=[UIFont fontWithName:@"geeza pro" size:15.0];
            
        }
        cell.lblQuoteText.font=f; 
        
        NSData *fontData=(NSData*) [UserDefaults GetValueFromUserDefaults:@"SelectedFont" ];
        UIFont *font;
        if (fontData!=nil) 
        {
            
            
            font = (UIFont*)[NSKeyedUnarchiver unarchiveObjectWithData:fontData];
            
            NSString *fontSize=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedFontSize"];
            if(fontSize!=nil && fontSize.length>0)
            {
                UIFont *f=[UIFont fontWithName:font.fontName size:(float)[fontSize floatValue]];
                font=f;
            }
            else
            {
                UIFont *f=[UIFont fontWithName:font.fontName size:15.0];
                font=f;
            }
            cell.lblQuoteText.font=font; 
            
        }
        
        NSData *colorData=(NSData*) [UserDefaults GetValueFromUserDefaults:@"QouteColor" ];
        UIColor *color;
        if (colorData!=nil) {
            color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            cell.lblQuoteText.textColor=color; 
        }
        else
        {
            cell.lblQuoteText.textColor=[UIColor whiteColor];
        }
        
        Quotes *quote=[self.qList objectAtIndex:indexPath.row];
        
        NSString *quoteText=[quote.quoteText stringByConvertingHTMLToPlainText]; 
        cell.lblQuoteText.text=quoteText;
        
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Quotes *quote=[self.qList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:[QuotesViewControllers quotesViewControllerWithQuotes:quote] animated:YES];
}

@end
