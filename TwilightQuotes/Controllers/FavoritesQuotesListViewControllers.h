//
//  FavoritesQuotesListViewControllers.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuotesLists.h"
#import "FavoriteQuoteList.h"
#import "QuotesCell.h"
#import "NSString+HTML.h"
#import "UserDefaults.h"
#import "QuotesViewControllers.h"
#import "ShowHideTabbar.h"
@interface FavoritesQuotesListViewControllers : UIViewController{
    FavoriteQuoteList *_favQList;
}
@property (strong, nonatomic) IBOutlet UITableView *fTableView;
@property(retain,nonatomic,readwrite) FavoriteQuoteList *favQList;
+(FavoritesQuotesListViewControllers*)favoriteQuotesListViewController;
@end
