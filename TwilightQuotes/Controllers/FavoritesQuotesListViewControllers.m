//
//  FavoritesQuotesListViewControllers.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FavoritesQuotesListViewControllers.h"


@implementation FavoritesQuotesListViewControllers
@synthesize fTableView;
@synthesize favQList;
+(FavoritesQuotesListViewControllers*)favoriteQuotesListViewController
{
    FavoritesQuotesListViewControllers *result = [[FavoritesQuotesListViewControllers alloc] initWithNibName:@"FavoritesQuotesListView" bundle:[NSBundle mainBundle]];
   // result.favQList=favQuoteList;
    return result;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"Favorites" ;
        self.tabBarItem.image=[UIImage imageNamed:@"favorite.png"];
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"Back"
                                              style:UIBarButtonItemStyleBordered
                                              target:nil
                                              action:nil];
        self.navigationItem.backBarButtonItem = backBarButtonItem;
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    favQList =[[FavoriteQuoteList alloc]init];
}

- (void)viewDidUnload
{
    [self setFTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    [ShowHideTabbar showTabBar:self.tabBarController];
    [self.fTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
   // NSLog(@"Count %i",[self.favQList GetFavoriteQuotes].count);
    return [self.favQList GetFavoriteQuotes].count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    QuotesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"QuotesCell" owner:self options:nil];
        
        for (id currentObject in topLevelObjects){
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell =  (QuotesCell *) currentObject;
                break;
            }
        }
        
        UIFont *f;
        NSString *fontSize=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedFontSize"];
        if(fontSize!=nil && fontSize.length>0)
        {
            f=[UIFont fontWithName:@"geeza pro" size:(float)[fontSize floatValue]];
            
        }
        else
        {
            f=[UIFont fontWithName:@"geeza pro" size:15.0];
            
        }
        cell.lblQuoteText.font=f; 
        
        NSData *fontData=(NSData*) [UserDefaults GetValueFromUserDefaults:@"SelectedFont" ];
        UIFont *font;
        if (fontData!=nil) 
        {
            
            
            font = (UIFont*)[NSKeyedUnarchiver unarchiveObjectWithData:fontData];
            
            NSString *fontSize=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedFontSize"];
            if(fontSize!=nil && fontSize.length>0)
            {
                UIFont *f=[UIFont fontWithName:font.fontName size:(float)[fontSize floatValue]];
                font=f;
            }
            else
            {
                UIFont *f=[UIFont fontWithName:font.fontName size:15.0];
                font=f;
            }
            cell.lblQuoteText.font=font; 
            
        }
        
        NSData *colorData=(NSData*) [UserDefaults GetValueFromUserDefaults:@"QouteColor" ];
        UIColor *color;
        if (colorData!=nil) {
            color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            cell.lblQuoteText.textColor=color; 
        }
        else
        {
            cell.lblQuoteText.textColor=[UIColor whiteColor];
        }
        
        Quotes *quote=[[self.favQList GetFavoriteQuotes] objectAtIndex:indexPath.row];
        
        NSString *quoteText=[quote.quoteText stringByConvertingHTMLToPlainText]; 
        cell.lblQuoteText.text=quoteText;
        
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     Quotes *quote=[[self.favQList GetFavoriteQuotes] objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:[QuotesViewControllers quotesViewControllerWithQuotes:quote] animated:YES];}

@end
