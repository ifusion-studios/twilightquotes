//
//  MoreViewControllers.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MoreViewControllers.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "FacebookClient.h"
#import "TwitterClient.h"
@interface MoreViewControllers() <FacebookClientDelegate,TwitterClientDelegate>
@end

@implementation MoreViewControllers
+(MoreViewControllers*)moreViewController
{
    MoreViewControllers *result = [[MoreViewControllers alloc] initWithNibName:@"MoreView" bundle:[NSBundle mainBundle]];
    
    return result;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"More";
        self.tabBarItem.image=[UIImage imageNamed:@"list.png"];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    facebookClient = [[FacebookClient alloc] initWithDelegate: self] ;
    twitterClient = [[TwitterClient alloc] initWithDelegate: self] ;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(IBAction)Share:(id)sender{
    
}
-(IBAction)Rate:(id)sender{
    
}
-(IBAction)Contact:(id)sender{ if ([MFMailComposeViewController canSendMail])
{
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    
    mailer.mailComposeDelegate = self;
    [mailer setSubject:@"Twilight Quotes Feedback"];
    NSArray *toRecipients = [NSArray arrayWithObjects:@"info@samcomtechnobrains.com", nil];
    [mailer setToRecipients:toRecipients];
    
    
    
    
   // [mailer setMessageBody:self.quote.quoteText isHTML:YES];
    [self presentModalViewController:mailer animated:YES];
    
    
}
else{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Device does not have any configured mail account." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Okay", nil];
    [alert show];
}
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark Sharing methods
- (IBAction)Share {
    
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:@"Share"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:nil];
    [menu addButtonWithTitle:@"Facebook"];                                         
    [menu addButtonWithTitle:@"Twitter"];
    
    [menu addButtonWithTitle:@"Email"];
    [menu addButtonWithTitle:@"Message"];
    menu.destructiveButtonIndex=[menu addButtonWithTitle:@"Cancel"];
    [menu showInView:self.navigationController.tabBarController.view];
    [menu setBounds:CGRectMake(0,0,320, 325)];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%d", buttonIndex); 
    
    
    if (buttonIndex == 0) 
    {
        //Facebook
        
        if([self shouldDisplayLoginForSocialClient:facebookClient]){
            [facebookClient login];
        }else{
            [facebookClient updateFacebookStatus:[NSString stringWithFormat:@"Hello"]] ;
        }
    }
    else if (buttonIndex == 1) 
    {
        
        if([self shouldDisplayLoginForSocialClient:twitterClient]){
            [twitterClient login]; 
        }else{
            //
            [twitterClient postMessageToTwitter: [NSString stringWithFormat:@"Hello"]] ;
        }
        
    }
    else if (buttonIndex == 2) 
    {
        //mail
        [self MailSend];
        
    }else if (buttonIndex == 3) 
    {
        //iMessage
        [self iMessage];
        
    }
}

-(void) iMessage
{
	MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
	if([MFMessageComposeViewController canSendText])
	{
		controller.body = @"Hello";
		controller.recipients = [NSArray arrayWithObjects: nil];
		controller.messageComposeDelegate = self;
        [self resignFirstResponder];
		[self presentModalViewController:controller animated:YES];
        
	}
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            [self dismissModalViewControllerAnimated:YES];
            break;
        case MessageComposeResultFailed:
            NSLog(@"Failed");
            break;
        case MessageComposeResultSent:
            NSLog(@"Sent");
            break;
        default:
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}
#pragma mark - Email Code

- (void)MailSend
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Twilight Quotes"];
        NSArray *toRecipients = [NSArray arrayWithObjects: nil];
        [mailer setToRecipients:toRecipients];
        
        
        
        
        [mailer setMessageBody:@"Hello" isHTML:YES];
        [self presentModalViewController:mailer animated:YES];
        
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Device does not have any configured mail account." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Okay", nil];
        [alert show];
    }
    
}


#pragma mark -
#pragma mark SocialClient delegate methods

- (BOOL)shouldDisplayLoginForSocialClient:(SocialClient *)client
{
    if([client serviceHasValidToken]){
        return NO;
    }
	return YES ;
}

- (void)socialClientAuthenticationDidSucceed:(SocialClient *)client
{
	NSLog(@"%@",[NSString stringWithFormat: @"Success: Authentication to %@ did succeed!", [client name]]);
    if([[client name] isEqualToString:@"Facebook"]){
        [facebookClient updateFacebookStatus:[NSString stringWithFormat:@"Hello"]] ;
    }
    if([[client name] isEqualToString:@"Twitter"]){
        [twitterClient postMessageToTwitter: [NSString stringWithFormat:@"Hello"]] ;    }
}

- (void)socialClient:(SocialClient *)client authenticationDidFailWithError:(NSError *)error
{
	NSLog(@"%@", [NSString stringWithFormat: @"Authentication to %@ did failed...", [client name]]) ;
}

#pragma mark -
#pragma mark TwitterClient delegate methods

- (void)twitterPostDidSucceedAndReturned:(NSMutableDictionary *)response
{
    
	NSLog( @"Twitter post succeded!" );
    [[ActivityIndicator currentIndicator] displayCompleted:@"Shared"];
}

- (void)twitterPostFailedWithError:(NSError *)error
{
	NSLog(@"%@",[NSString stringWithFormat: @"Twitter post failed...\nError: %@", [[error userInfo] objectForKey: NSLocalizedDescriptionKey]]) ;
}
#pragma mark -
#pragma mark FcaebookClient delegate methods

-(void)facebookGotResponse:(NSMutableDictionary *)response forRequestType:(FacebookRequestType)requestType
{
    
	NSLog( @"Facebook post succeded!" );
    [[ActivityIndicator currentIndicator] displayCompleted:@"Shared"];
}

- (void)facebookPost:(FacebookPostType)postType failedWithError:(NSError *)error
{
	NSLog(@"%@",[NSString stringWithFormat: @"Facebook post failed...\nError: %@", [[error userInfo] objectForKey: NSLocalizedDescriptionKey]]) ;
}

@end
