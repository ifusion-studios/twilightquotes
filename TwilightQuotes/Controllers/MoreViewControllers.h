//
//  MoreViewControllers.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ActivityIndicator.h"
#import "SocialClient.h"
@class FacebookClient ;
@class TwitterClient ;
@interface MoreViewControllers : UIViewController<MFMailComposeViewControllerDelegate,SocialClientDelegate,MFMessageComposeViewControllerDelegate,UIActionSheetDelegate>{
    FacebookClient *facebookClient ;
    TwitterClient *twitterClient ;
}
+(MoreViewControllers*)moreViewController;
-(IBAction)Share:(id)sender;
-(IBAction)Rate:(id)sender;
-(IBAction)Contact:(id)sender;
-(void) iMessage;
- (void)MailSend;
@end
