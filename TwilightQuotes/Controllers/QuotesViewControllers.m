//
//  QuotesViewControllers.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QuotesViewControllers.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "FacebookClient.h"
#import "TwitterClient.h"
#import "ShowHideTabbar.h"
@interface QuotesViewControllers() <FacebookClientDelegate,TwitterClientDelegate>
@end

@implementation QuotesViewControllers
@synthesize quote,backimage;
@synthesize index = _index;
@synthesize addFav,removeFav;
+(QuotesViewControllers*)quotesViewControllerWithQuotes:(Quotes*)quote
{
    QuotesViewControllers *result = [[QuotesViewControllers alloc] initWithNibName:@"QuotesView" bundle:[NSBundle mainBundle]];
    result.quote=quote;
    result.index = [[QuotesLists Instance] indexOfQuote:quote];
    return result;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"Quotes";
        //self.tabBarItem.image=[UIImage imageNamed:@"list.png"];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    facebookClient = [[FacebookClient alloc] initWithDelegate: self] ;
    twitterClient = [[TwitterClient alloc] initWithDelegate: self] ;
    [ShowHideTabbar hideTabBar:self.tabBarController];
    [self BindData];
   // qList=[[QuoteList alloc]init];
    [self swipe];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    UIFont *f;
    NSString *fontSize=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedFontSize"];
    if(fontSize!=nil && fontSize.length>0)
    {
        f=[UIFont fontWithName:@"geeza pro" size:(float)[fontSize floatValue]];
        
    }
    else
    {
        f=[UIFont fontWithName:@"geeza pro" size:15.0];
        
    }
    txtQuote.font=f; 
    
    NSData *fontData=(NSData*) [UserDefaults GetValueFromUserDefaults:@"SelectedFont" ];
    UIFont *font;
    if (fontData!=nil) 
    {
        
        
        font = (UIFont*)[NSKeyedUnarchiver unarchiveObjectWithData:fontData];
        
        NSString *fontSize=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedFontSize"];
        if(fontSize!=nil && fontSize.length>0)
        {
            UIFont *f=[UIFont fontWithName:font.fontName size:(float)[fontSize floatValue]];
            font=f;
        }
        else
        {
            UIFont *f=[UIFont fontWithName:font.fontName size:15.0];
            font=f;
        }
        txtQuote.font=font; 
        
    }
    
    NSData *colorData=(NSData*) [UserDefaults GetValueFromUserDefaults:@"QouteColor" ];
    UIColor *color;
    if (colorData!=nil) {
        color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        txtQuote.textColor=color; 
    }
    else
    {
        txtQuote.textColor=[UIColor whiteColor];
    }
    NSString* Selimage= (NSString*)[UserDefaults GetValueFromUserDefaults:@"SelectedBackground" ];
    int selimg=(int)[Selimage intValue];
    if (selimg>0) 
    {
        
        
        
        UIImage* tabitem = [UIImage imageNamed:[NSString stringWithFormat:@"i%i.jpg",selimg]];
        backimage.image=tabitem;
    }
    else
    {
        UIImage* tabitem = [UIImage imageNamed:@"i1.jpg"];
        backimage.image=tabitem;
    }

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark - Swipe Code

- (IBAction)handleSwipeLeft:(UISwipeGestureRecognizer *)sender
{
    self.index = self.index - 1 < 0 ? 0 : self.index - 1;
    self.quote=[[QuotesLists Instance] quoteAtIndex:self.index];
    [self BindData];
    NSLog(@"Left %d", self.index);
    //previous Quotes
//    sm.SelectedQuote= [[Utilities Instance] GetQuoteById:sm.SelectedQuote.QuotesID-1];
//    [self BindData];
    
}

- (IBAction)handleSwipeRight:(UISwipeGestureRecognizer *)sender
{
    //next quotes
    self.index = self.index + 1 >= [[QuotesLists Instance] loadedCount] ? [[QuotesLists Instance] loadedCount] - 1 : self.index + 1;
    NSLog(@"Right %d", self.index);
   self.quote=[[QuotesLists Instance] quoteAtIndex:	self.index];
    [self BindData];
//    sm.SelectedQuote= [[Utilities Instance] GetQuoteById:sm.SelectedQuote.QuotesID+1];
//    [self BindData];
    
}
-(void)swipe
{
    UISwipeGestureRecognizer *recognizer;
    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:recognizer];
    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [[self view] addGestureRecognizer:recognizer];
}
-(void)BindData{
    
    [txtQuote setValue:self.quote.quoteText forKey:@"contentToHTMLString"];
    FavoriteQuoteList *fav = [[FavoriteQuoteList alloc] init];
    if([fav isFavorite:self.quote]){
        self.addFav.hidden = YES;
        self.removeFav.hidden = NO;
    }else{
        self.removeFav.hidden = YES;
        self.addFav.hidden = NO;
    }

}
-(IBAction)addFavorites{
    
    
    [FavoriteQuoteList addAndRemoveToFavorites:self.quote];
    addFav.hidden=YES;
    removeFav.hidden=NO;
    
    
}
-(IBAction)removeFavorites{
    
    [FavoriteQuoteList addAndRemoveToFavorites:self.quote];
    addFav.hidden=NO;
    removeFav.hidden=YES;
}
#pragma mark -
#pragma mark Sharing methods
- (IBAction)Share {
    
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:@"Share"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:nil];
    [menu addButtonWithTitle:@"Facebook"];                                         
    [menu addButtonWithTitle:@"Twitter"];
    
    [menu addButtonWithTitle:@"Email"];
    [menu addButtonWithTitle:@"Message"];
    menu.destructiveButtonIndex=[menu addButtonWithTitle:@"Cancel"];
    [menu showInView:self.navigationController.tabBarController.view];
    [menu setBounds:CGRectMake(0,0,320, 325)];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%d", buttonIndex); 
    
    
    if (buttonIndex == 0) 
    {
        //Facebook
        
        if([self shouldDisplayLoginForSocialClient:facebookClient]){
            [facebookClient login];
        }else{
            [facebookClient updateFacebookStatus:[NSString stringWithFormat:@"%@",txtQuote.text]] ;
        }
           }
    else if (buttonIndex == 1) 
    {
        
        if([self shouldDisplayLoginForSocialClient:twitterClient]){
            [twitterClient login]; 
        }else{
            //
            [twitterClient postMessageToTwitter: [NSString stringWithFormat:@"%@",txtQuote.text]] ;
        }

    }
    else if (buttonIndex == 2) 
    {
        //mail
        [self MailSend];
        
    }else if (buttonIndex == 3) 
    {
        //iMessage
        [self iMessage];
        
    }
}

-(void) iMessage
{
	MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
	if([MFMessageComposeViewController canSendText])
	{
		controller.body = self.quote.quoteText;
		controller.recipients = [NSArray arrayWithObjects: nil];
		controller.messageComposeDelegate = self;
        [self resignFirstResponder];
		[self presentModalViewController:controller animated:YES];
        
	}
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            [self dismissModalViewControllerAnimated:YES];
            break;
        case MessageComposeResultFailed:
            NSLog(@"Failed");
            break;
        case MessageComposeResultSent:
            NSLog(@"Sent");
            break;
        default:
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}
#pragma mark - Email Code

- (void)MailSend
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Twilight Quotes"];
        NSArray *toRecipients = [NSArray arrayWithObjects: nil];
        [mailer setToRecipients:toRecipients];
        
        
       
        
        [mailer setMessageBody:self.quote.quoteText isHTML:YES];
        [self presentModalViewController:mailer animated:YES];
        
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Device does not have any configured mail account." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Okay", nil];
        [alert show];
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark SocialClient delegate methods

- (BOOL)shouldDisplayLoginForSocialClient:(SocialClient *)client
{
    if([client serviceHasValidToken]){
        return NO;
    }
	return YES ;
}

- (void)socialClientAuthenticationDidSucceed:(SocialClient *)client
{
	NSLog(@"%@",[NSString stringWithFormat: @"Success: Authentication to %@ did succeed!", [client name]]);
    if([[client name] isEqualToString:@"Facebook"]){
        [facebookClient updateFacebookStatus:[NSString stringWithFormat:@"%@",txtQuote.text]] ;
    }
    if([[client name] isEqualToString:@"Twitter"]){
        [twitterClient postMessageToTwitter: [NSString stringWithFormat:@"%@",txtQuote.text]] ;    }
}

- (void)socialClient:(SocialClient *)client authenticationDidFailWithError:(NSError *)error
{
	NSLog(@"%@", [NSString stringWithFormat: @"Authentication to %@ did failed...", [client name]]) ;
}

#pragma mark -
#pragma mark TwitterClient delegate methods

- (void)twitterPostDidSucceedAndReturned:(NSMutableDictionary *)response
{
    
	NSLog( @"Twitter post succeded!" );
    [[ActivityIndicator currentIndicator] displayCompleted:@"Shared"];
}

- (void)twitterPostFailedWithError:(NSError *)error
{
	NSLog(@"%@",[NSString stringWithFormat: @"Twitter post failed...\nError: %@", [[error userInfo] objectForKey: NSLocalizedDescriptionKey]]) ;
}
#pragma mark -
#pragma mark FcaebookClient delegate methods

-(void)facebookGotResponse:(NSMutableDictionary *)response forRequestType:(FacebookRequestType)requestType
{
    
	NSLog( @"Facebook post succeded!" );
    [[ActivityIndicator currentIndicator] displayCompleted:@"Shared"];
}

- (void)facebookPost:(FacebookPostType)postType failedWithError:(NSError *)error
{
	NSLog(@"%@",[NSString stringWithFormat: @"Facebook post failed...\nError: %@", [[error userInfo] objectForKey: NSLocalizedDescriptionKey]]) ;
}

@end
