//
//  CharactorWiseQuoteListViewControllers.h
//  TwilightQuotes
//
//  Created by Samir Kha on 03/09/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowHideTabbar.h"
#import "QuotesLists.h"
#import "QuotesCell.h"
#import "Quotes.h"
#import "QuotesViewControllers.h"
#import "UserDefaults.h"
#import "NSString+HTML.h"

@interface CharactorWiseQuoteListViewControllers :UIViewController{
    
    
    
}
@property(nonatomic,retain,readwrite) NSMutableArray *qList;
+(CharactorWiseQuoteListViewControllers*)quotesListViewControllerWithQuoteList:(NSMutableArray*)quoteList;
@end
