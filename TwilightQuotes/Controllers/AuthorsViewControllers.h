//
//  AuthorsViewControllers.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuotesLists.h"
#import "CharactorWiseQuoteListViewControllers.h"
@interface AuthorsViewControllers : UIViewController{
    QuotesLists *qList;
}
+(AuthorsViewControllers*)authorsViewController;
-(IBAction)BellaClick;
-(IBAction)EdwardClick;
-(IBAction)JacobClick;
-(IBAction)JamesClick;
-(IBAction)CharlieClick;
-(IBAction)MikeClick;
-(IBAction)JessicaClick;
-(IBAction)AliceClick;
@end
