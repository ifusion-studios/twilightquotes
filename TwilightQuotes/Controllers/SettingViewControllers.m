//
//  SettingViewControllers.m
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingViewControllers.h"

@implementation SettingViewControllers

#pragma mark - Properties
@synthesize selectedFont,selectedImage,colorQuote,selectedImg,val;

+(SettingViewControllers*)settingViewController
{
    SettingViewControllers *result = [[SettingViewControllers alloc] initWithNibName:@"SettingsView" bundle:[NSBundle mainBundle]];
    
    return result;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"Settings";
        self.tabBarItem.image=[UIImage imageNamed:@"Setting.png"];
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.selectedImg.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [self.selectedImg.layer setBorderWidth: 2.0];
   
 
    NSData *fontData=(NSData*) [UserDefaults GetValueFromUserDefaults:@"SelectedFont" ];
    UIFont *font;
    if (fontData!=nil) {
        font = (UIFont*)[NSKeyedUnarchiver unarchiveObjectWithData:fontData];
        selectedFont.font=font; 
        
    }
    NSString *Size=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedFontSize"];
    fontSize.value=(int)[Size intValue];
    
    NSString* Selimage=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedBackground" ];
    int selimg=(int)[Selimage intValue];
    if (selimg>0) 
    {
        
        
        
        UIImage* tabitem = [UIImage imageNamed:[NSString stringWithFormat:@"i%i.jpg",selimg]];
        self.selectedImg.image=tabitem;
    }
    else
    {
        UIImage* tabitem = [UIImage imageNamed:@"i1.jpg"];
        self.selectedImg.image=tabitem;
    }
    
    
#ifdef IPHONE_COLOR_PICKER_SAVE_DEFAULT
   
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"SwatchColor"];
    UIColor *color;
    if (colorData!=nil) {
       
        color = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    } else {
        
        color = [UIColor grayColor];
        
        colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
     
        [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"SwatchColor"];
    }
    // Set the swatch color
   
        colorQuote.backgroundColor = color;
    
#else
      NSData *colorData= (NSData*)[UserDefaults GetValueFromUserDefaults:@"QouteColor" ];
    UIColor *color;
    if (colorData!=nil) {
        color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        colorQuote.backgroundColor = color; 
    }
    else
    {
        colorQuote.backgroundColor=[UIColor whiteColor];
    }
#endif
    // Do any additional setup after loading the view from its nib.
}


- (void)viewDidUnload
{
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    NSData *fontData= (NSData*)[UserDefaults GetValueFromUserDefaults:@"SelectedFont" ];
    UIFont *font;
    if (fontData!=nil) {
        font = (UIFont*)[NSKeyedUnarchiver unarchiveObjectWithData:fontData];
        selectedFont.font=font; 
        
    }
    NSString* Selimage=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedBackground" ];
    int selimg=(int)[Selimage intValue];
    if (selimg>0) 
    {
        
        
        
        UIImage* tabitem = [UIImage imageNamed:[NSString stringWithFormat:@"i%i.jpg",selimg]];
        self.selectedImg.image=tabitem;
    }
    else
    {
        UIImage* tabitem = [UIImage imageNamed:@"i1.jpg"];
        self.selectedImg.image=tabitem;
    }
    NSString *Size=(NSString*) [UserDefaults GetValueFromUserDefaults:@"SelectedFontSize"];
    if(Size!=nil && Size.length>0)
    {
        fontSize.value=(int)[Size intValue];
	}
    else
    {
        fontSize.value=15;
    }
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark - Custom Methods
-(IBAction)selectColorofQuotes{
    
    ColorPickerViewController *colorPickerViewController = 
    [[ColorPickerViewController alloc] initWithNibName:@"ColorPickerViewController" bundle:nil];
    colorPickerViewController.delegate = self;
#ifdef IPHONE_COLOR_PICKER_SAVE_DEFAULT
    colorPickerViewController.defaultsKey = @"SwatchColor";
#else
       
    colorPickerViewController.defaultsColor = colorQuote.backgroundColor;
    
#endif
    [self presentModalViewController:colorPickerViewController animated:YES];
    
}
- (void)colorPickerViewController:(ColorPickerViewController *)colorPicker didSelectColor:(UIColor *)color {
    
    colorQuote.backgroundColor = color;
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
    [UserDefaults WriteInUserDefaults:@"QouteColor" :colorData];    
    
    [colorPicker dismissModalViewControllerAnimated:YES];
}
-(IBAction)selectFont
{
    CMFontSelectTableViewController *fontPick=[[CMFontSelectTableViewController alloc] initWithNibName:@"CMFontSelectTableViewController" bundle:nil];
    [self.navigationController pushViewController:fontPick animated:YES];
    
}
-(IBAction)SizeChange:(id)sender{
    
    val=[[NSString alloc]initWithFormat:@"%i" ,(int)fontSize.value];
    [UserDefaults WriteInUserDefaults:@"SelectedFontSize" :val];
    
}
-(IBAction)LibraryPicture
{
    JPImagePickerController *imagePickerController = [[JPImagePickerController alloc] init];
    
    imagePickerController.delegate = self;
    imagePickerController.dataSource = self;
    imagePickerController.imagePickerTitle = @"ImagePicker";
    
    [self.navigationController presentModalViewController:imagePickerController animated:YES];
}

- (void)imagePickerDidCancel:(JPImagePickerController *)picker {
	[self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)imagePicker:(JPImagePickerController *)picker didFinishPickingWithImageNumber:(NSInteger)imageNumber {
	//userdefault store
    imgno=imageNumber+1;
    val=[NSString stringWithFormat:@"%d", imgno];
    [UserDefaults WriteInUserDefaults:@"SelectedBackground" :val];
    
    [self.navigationController dismissModalViewControllerAnimated:YES];
}
- (NSInteger)numberOfImagesInImagePicker:(JPImagePickerController *)picker {
	return 7;
}

- (UIImage *)imagePicker:(JPImagePickerController *)picker thumbnailForImageNumber:(NSInteger)imageNumber {
	return [UIImage imageNamed:[NSString stringWithFormat:@"i%i.jpg", (imageNumber ) + 1]];
}

- (UIImage *)imagePicker:(JPImagePickerController *)imagePicker imageForImageNumber:(NSInteger)imageNumber {
	return [UIImage imageNamed:[NSString stringWithFormat:@"i%i.jpg", (imageNumber ) + 1]];
}


@end
