//
//  QuotesListViewControllers.h
//  TwilightQuotes
//
//  Created by Samir Kha on 31/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowHideTabbar.h"
#import "QuotesLists.h"
#import "QuotesCell.h"
#import "Quotes.h"
#import "QuotesViewControllers.h"
#import "UserDefaults.h"
#import "NSString+HTML.h"


@interface QuotesListViewControllers : UIViewController{
    QuotesCell *cell;
   
}
@property (strong, nonatomic) IBOutlet UITableView *quoteTable;
@property(nonatomic,retain,readwrite) QuotesLists *qList;
+(QuotesListViewControllers*)quotesListViewControllerWithQuoteList:(QuotesLists*)quoteList;
-(void)Formating;
@end
